// Classes

// in JS , classes can be created using the "class" keyword and {}.
//Naming convention for classes:begin with uppercase chracters

// class Student {
//     constructor(name, email) {
//         //propertyName = value;
//         this.name = name;
//         this.email = email;
//     }
// }
// Instantiation - process of creating an object from a class
// To create an object from a class, use the "new" keyword, when a class has a constrcutor, we need to suppy ALL the values needed by the constructor
// let studentOne = new Student('John', 'john@mail.com');
// console.log(studentOne);

// Create a new class called Person. It should instantiate a new object with the following fields: name, age , nationality, and address. 



// Instantiate two new objects from the Person class as person1 and person2. 

class Person {
    constructor(name, age, nationality, address) {
        this.name = name;
        this.nationality = nationality;
        this.address = address;
        this.age = (typeof age === 'number' && age >= 18) ? age : undefined;
    }
}

//Instantiate two new objects from the Person class as person1 and person2. 
let person1 = new Person('Jessie', 20, 'American', '123 Main St');
let person2 = new Person('Jane', 15, 'Canadian', '456 Main St');

console.log(person1);
console.log(person2);


// ===QUIZ
// #1 What is the blueprint where objects are created from?
// Answer: class

// #2 What is the naming convention applied to classes?
// Answer: PascalCase

// #3 WWhat keyword do we use to create objects from a class?
// Answer: new

// #4 What is the technical term for creating an object from a class?
// Answer: instantiation

// #5 What class method dictates HOW objects will be created from that class?
// Answer: constructor

//===FUNCTION CODING

// #1
class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;
        this.pass = undefined ;
        this.passWithHonors = undefined;
        this.grades = (grades instanceof Array && grades.length === 4 && grades.every(grade => grade >= 0 && grade <= 100)) ? grades : undefined;
    }
    login() {
        console.log('Logging in ' + this.email);
        return this;
    }
    logout() {
        console.log('Logging out ' + this.email);
        return this;
    }
    listGrades() {
        console.log(`${this.name} grades: ${this.grades}`);
        return this;
    }
    computeAve() {
        let sum = 0;
        for (let i = 0; i < this.grades.length; i++) {
            sum += this.grades[i];
        }
        this.gradeAve = sum / this.grades.length;
        return this;
    }
    
    willPass() {
        this.pass = this.computeAve() >= 85 ? true : false;
        return this;
        
    }
    willPassWithHonors() {
        let value = this.computeAve().gradeAve;
        if (value >= 90) {
            this.passWithHonors = true;
        } else if (value <= 85) {
            this.passWithHonors = false;
        } else {
            this.passWithHonors = undefined;
        }
        return this;
    }
}

// check if the grades property is set to undefined
let studentRef = new Student('John', 'john@mail.com', [101, 84, 78, 88]);
// let studentRef = new Student('John', 'john@mail.com', ['hello', 84, 78, 88]);
console.log(studentRef);

//#2
// Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

// console.log(studentFour.listGrades());
//to update values of properties
// console.log(studentFour.email = 'sample@mail.com');
// console.log(studentFour);


// using getter and setter
// Best Practice dictates that we regulate access to such properties. We do via use of "getter" (regulates retrieval) and setter (regulates manipulation).


// Method chaining - calling multiple methods on the same object in a single line of code.
// console.log(studentFour.listGrades().willPass()); 
// console.log(studentOne.login().computeAve().logout());
//----------------------------------------------------------

// QUIZ 2
// #1 Should class methods be included in the class constructor?
// Answer: No
// #2 Can class methodsCan class methods be separated by commas?
// Answer:No
// #3 Can we update an object’s properties via dot notation?
// Answer: Yes
// #4What do you call the methods used to regulate access to an object’s properties?
// Answer: getter and setter
// #5 What does a method need to return in order for it to be chainable?
// Answer: this

//======FUNC CODING
//#1 Modify the Student class to allow the willPass() and willPassWithHonors() methods to be chainable.

console.log(studentOne.login().computeAve().willPass().willPassWithHonors());